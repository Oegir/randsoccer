<?php
// Application configuration
$config = [
    'base_path' => __DIR__ . '/../',
    'data_providers' => [
        'namespace' => 'app\\dataProviders',
        'suffix' => 'DataProvider',
        'classes' => [
            'mysql' => [
                'server' => 'localhost',
                'user' => 'user',
                'pass' => '12345',
                'database' => 'mirafox',
            ],
            'randomize' => [
                'max_luck_factor' => 1000,
                'min_luck_factor' => 150,
                'goal_min_percent' => 10,
                'reduce_factor' => 1.5,
                'max_goals' => 10,
            ],
        ],
    ],
    'controllers' => [
        'namespace' => 'app\\controllers',
        'suffix' => 'Controller',
        'classes' => [
            'default' => [
                'model' => 'playoff'
            ]
        ],
    ],
    'models' => [
        'namespace' => 'app\\models',
        'suffix' => 'Model',
        'classes' => [
            'playoff' => [
                'html_load' => [
                    'row_lenth' => 14,  // num of cells in rows of source table
                    'max_rows' => 32,   // num or rows from data source table
                ],
                'groups' => ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'],   // names of play groups
            ],
        ],
    ],
    'views' => [
        'namespace' => 'app\\views',
        'suffix' => 'View',
    ],
];
// Class autoloading
spl_autoload_register(function ($class_name) use($config) {
    $class_path = str_replace('\\', '/', $config['base_path'] . $class_name . '.php');
    include_once $class_path;
});

return $config;