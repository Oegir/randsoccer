<?php
namespace app;

/**
 *
 * @author oegir
 *
 */
class Application
{
    /**
     * @var array Applicatin configuration
     */
    private static $_config;

    /**
     * @var array Request params
     */
    private static $_params;

    /**
     * Loads POST and GET user reques to class property
     */
    private static function _loadRequst()
    {
        static::_loadRequesValues('get', $_GET);
        static::_loadRequesValues('post', $_POST);

        foreach ($_FILES as $file) {
            if ($file['error'] == 0 && $file['type'] == 'text/html' && $file['size'] <= 30000) {
                static::$_params['files'][static::clearStr($file['name'])] = $file['tmp_name'];
            }
        }
    }

    /**
     * Processes and assigns values one after the other from the received query type
     *
     * @param string $key
     * @param string $value
     */
    private static function _loadRequesValues($key, $value)
    {
        foreach ($value as $req_param => $req_value) {

            if (is_array($req_value)) {

                foreach ($req_value as $req_nested_value) {

                    if (is_string($req_param) && is_string($req_nested_value)) {
                        static::$_params[$key][static::clearStr($req_param)][] = static::clearStr($req_nested_value);
                    }
                }
            } elseif (is_string($req_param) && is_string($req_value)) {
                static::$_params[$key][static::clearStr($req_param)] = static::clearStr($req_value);
            }

        }

    }

    /**
     * Clears string from request
     * @param string str
     */
    public static function clearStr($str)
    {
        $str = trim($str);
        $str = strip_tags($str);
        $str = stripslashes($str);
        $str = htmlspecialchars($str);

        switch ($str) {
            case 'false':
                return false;
            case 'true':
                return true;
            default:
                return $str;
        }

    }

    /**
     * Start point of application
     *
     * @param array $config Applicatin configuration
     */
    public static function execute($config)
    {
        static::$_config = $config;
        static::_loadRequst();

        $controller_name = static::getGetValue('c', 'default');
        $controller = static::loadObjectWithParams($controller_name, 'controllers');
        $controller->execute();
    }

    /**
     * Returns a value from fpplication config
     *
     * @param string $what
     * @return NULL|mixed
     */
    public static function getConfigValue($what)
    {
        return isset(static::$_config[$what]) ? static::$_config[$what] : Null;
    }

    public static function getFiles()
    {
        return isset(static::$_params['files']) ? static::$_params['files'] : Null;
    }

    /**
     * Returns a value from GET user request
     *
     * @param string $what
     * @param mixed $default
     * @return string|mixed
     */
    public static function getGetValue($what, $default = Null)
    {
        return isset(static::$_params['get'][$what]) ? static::$_params['get'][$what] : $default;
    }

    /**
     * Returns a value from POST user request
     *
     * @param string $what
     * @param mixed $default
     * @return string|mixed
     */
    public static function getPostValue($what, $default = Null)
    {
        return isset(static::$_params['post'][$what]) ? static::$_params['post'][$what] : $default;
    }

    /**
     * Constructs URLs for links and redirects
     * @param string $contorller
     * @param string $action
     */
    public static function getUrl($controller='', $action='')
    {
        $url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
        $url = preg_replace('/(index\.php)?(\?.*)?$/', '', $url);

        if (!empty($controller)) {
            $url .= 'index.php?c=' . urlencode($controller);

            if (!empty($action)) {
                $url .= '&a=' . urlencode($action);
            }
        } elseif (!empty($action)) {
            $url .= 'index.php?a=' . urlencode($action);
        }
        return $url;
    }

    /**
     * Returns a new class object initialized with parameters
     *
     * @param string $nameSpace
     * @param string $classname
     * @param string $suffix Optional
     * @return mixed
     */
    public static function loadObjectWithParams($class_name, $config_key = '', $params = [])
    {
        $name_space = '';
        $suffix = '';

        if (!is_array($params)) {
            $params = [$params];
        }
        if (!empty($config_key)) {
            $config = static::getConfigValue($config_key);
            // Find out the full name of the class
            if (!is_null($config)) {
                $name_space = isset($config['namespace']) ? $config['namespace'] . '\\' : '';
                $suffix = isset($config['suffix']) ? $config['suffix'] : '';
            }
            // Receive configuration parameters
            if (isset($config['classes'][$class_name])) {
                $params += $config['classes'][$class_name];
            }
        }
        $full_class_name = $name_space . ucfirst(strtolower($class_name)) . $suffix;

        return new $full_class_name($params);
    }
}

