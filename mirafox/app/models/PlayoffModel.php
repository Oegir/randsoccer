<?php
namespace app\models;

use app\Application;
use Exception;

/**
 *
 * @author oegir
 *
 */
class PlayoffModel extends Model
{
    /**
     * @var array Coomands splitted by groups
     */
    private $_byGroups = array();

    /**
     * @var array Data about coomands
     */
    private $_data = array();

    /**
     * @var int Current part of final
     */
    private $_final = Null;

    /**
     * Notes the teams that came out in the 1/8 finals
     * @param array $groups
     * @throws Exception
     */
    private function _markEighth($groups)
    {
        // Get playoffers
        $winners = array();

        foreach ($this->params['groups'] as $group_name) {

            usort($groups[$group_name], function ($a, $b) {
                // Subtraction in reverse order, so winners would be higher
                $result = 10000 * ($b->score - $a->score);

                if ($result == 0) {
                    $result = 100 * (($b->scored - $b->passed) - ($a->scored - $a->passed));
                }
                if ($result == 0) {
                    $result = $b->scored - $a->scored;
                }
                return $result;
            });

            for ($i = 0; $i < count($groups[$group_name]); $i++) {

                if ($i == 0 || $i == 1) {
                    $groups[$group_name][$i]->phase = '8';
                }
            }
            $this->_byGroups[$group_name] = $groups[$group_name];
        }

        if ($this->storeData('mysql', ['winners' => $this->_data])) {
            $this->addMessage('Групповые матчи сыграны');
        } else {
            throw new Exception('Error while playing rounds');
        }
    }

    /**
     * Clears historical data for team games
     */
    public function clearHistorical()
    {
        $this->storeData('mysql', ['clearHistorical' => Null]);
    }

    /**
     * Split commands for groups
     * @return boolean
     */
    public function drawCommands()
    {
        $this->_data = $this->getHistorical();
        $count_data = count($this->_data);

        if ($count_data == 0) {
            return false;
        }
        if (!shuffle($this->_data)) {
            return false;
        }
        // Split commands to groups
        $to_save = array();
        $by_groups = array();
        $index = 0;

        while ($index < $count_data) {

            foreach ($this->params['groups'] as $group) {
                $by_groups[$group][] = $this->_data[$index];
                $r_save = array();

                $r_save['id'] = $this->_data[$index]->id;
                $r_save['command'] = $this->_data[$index]->command;
                $r_save['strength'] = $this->_data[$index]->strength;
                $r_save['group'] = $group;

                $to_save[] = $r_save;
                $index++;

                if (!($index < $count_data)) {
                    break;
                }
            }
        }
        $this->_byGroups = $by_groups;

        if ($this->storeData('mysql', ['draw' => $to_save])) {
            $this->addMessage('Жеребьевка проведена');
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns commands splitted by groups
     * @return array
     */
    public function getByGroups()
    {
        if (empty($this->_byGroups)) {

            if (empty($this->_data)) {
                $this->_data = $this->getData('mysql', 'championship');
            }

            foreach ($this->_data as $row) {

                if (!isset($row->group)) {
                    break;
                }
                $this->_byGroups[$row->group][] = $row;
            }
        }
        ksort($this->_byGroups);
        return $this->_byGroups;
    }

    /**
     * Returns supported encodings
     * @return string[]
     */
    public function getEncodings()
    {
        return ['windows-1251', 'utf-8'];
    }

    /**
     * Returns current part of final
     * @return int
     */
    public function getFinal()
    {
        if (!is_null($this->_final)) {
            return $this->_final;
        }
        $this->_data = $this->getData('mysql', 'finalists');
        $count_data = count($this->_data);

        if ($count_data == 1) {
            $this->addMessage('Результаты финала');
            return true;
        } elseif ($count_data == 0 || $count_data % 2 != 0) {
            return false;
        }
        // Playing final
        $this->_final = $this->_data[0]->phase;

        return $this->_final;
    }

    /**
     * Returns data about last uploaded file
     * @return array|null
     */
    public function getHistorical()
    {
        return $this->getData('mysql', 'historical');
    }

    public function getList()
    {
        return $this->_data;
    }

    /**
     * Loads data from a file and saves it to the database
     * @param array $params
     */
    public function loadSourceData($params)
    {
        $params['encoding'] = $this->getEncodings()[$params['encoding']];
        $params += $this->params['html_load'];
        $this->_data = $this->getData('html', $params);
        // Preparing data to save
        $historical = array();
        $min_strength = 0;
        $max_strength = 0;

        foreach ($this->_data as $key => $row) {
            $h_row = array();

            $h_row['command'] = Application::clearStr($row[1]);
            $h_row['games'] = (int) $row[3];

            $games_score = preg_match('/(\d+)\s*-\s*(\d+)/', $row[7], $matches);
            $h_row['scored'] = isset($matches[1]) ? $matches[1] : 0;
            $h_row['passed'] = isset($matches[2]) ? $matches[2] : 0;

            $h_row['wins'] = (int) $row[4];
            $h_row['draws'] = (int) $row[5];
            $h_row['fails'] = (int) $row[6];
            $h_row['score'] = (int) $row[9];
            $h_row['strength'] = str_replace(',', '.', $h_row['games'] != 0 ? ($h_row['scored'] - $h_row['passed']) / $h_row['games'] : 0);

            $this->_data[$key] = $h_row;

            if ($h_row['strength'] < $min_strength) {
                $min_strength = $h_row['strength'];
            }
            if ($h_row['strength'] > $max_strength) {
                $max_strength = $h_row['strength'];
            }
        }
        // Calculate the strength of players in percent
        $bias = -1 * $min_strength;
        $max_strength += $bias;

        foreach ($this->_data as $key => $row) {
            $this->_data[$key]['strength'] = ($row['strength'] + $bias) / $max_strength * 100;
        }
        // Saving data
        if (count($this->_data[$key]) > 0) {

            if ($this->storeData('mysql', ['historical' => $this->_data])) {
                $this->addMessage('Данные успешно загружены');
            } else {
                $this->addMessage('Не удалось сохранить данные в базу');
            }
        } else {
            $this->addMessage('Не удалось прочитать данные из файла');
        }
    }

    /**
     * Plays a part of final
     * @throws Exception
     * @return boolean
     */
    public function playFinal()
    {
        $final = $this->getFinal();

        if (is_bool($final)) {
            return $final;
        }
        if ($final > 1) {
            shuffle($this->_data);
        }

        for ($i = 0; $i < count($this->_data); $i += 2) {
            $this->storeData('randomize', [$this->_data[$i], $this->_data[$i + 1]]);
            $score = $this->getData('randomize', 'final');

            if ($score[0] > $score[1]) {
                $this->_data[$i]->phase /= 2;
            } else {
                $this->_data[$i + 1]->phase /= 2;
            }
            $this->_data[$i]->goals = $score[0];
            $this->_data[$i + 1]->goals = $score[1];
        }

        if ($this->storeData('mysql', ['winners' => $this->_data])) {
            $this->addMessage('Результаты ' . ($this->_final != 1 ? '1/' . $this->_final : '') . ' финала');
        } else {
            throw new Exception('Error while playing final');
        }
        return true;
    }

    /**
     * Launches game rounds
     * @throws Exception
     */
    public function playRounds()
    {
        $this->_data = $this->getData('mysql', 'rounds');
        // Play rounds only at the beginning of the championship
        if (count($this->_data) < $this->params['html_load']['max_rows']) {
            return false;
        }
        $queue = $this->_data;
        $groups = array();
        // Count the games in groups, iteratively removing from the queue the players that played all games
        while (count($queue) > 0) {
            $currents = array();

            foreach ($queue as $key => $command) {
                // Remember the "current" item
                if (!array_key_exists($command->group, $currents)) {
                    $currents[$command->group] = $command;
                    unset($queue[$key]);
                    continue;
                }
                // Calculate results of the game
                $this->storeData('randomize', [$currents[$command->group], $command]);
                $this->getData('randomize', 'round');

            }
            // In passing split players into groups
            foreach ($currents as $played_all) {
                $groups[$played_all->group][] = $played_all;
            }
        }
        $this->_markEighth($groups);
        return true;
    }

    /**
     * Restarts the draw from the 1/8 finals
     */
    public function restartFinal()
    {
        $this->storeData('mysql', ['testart-final' => '']);
    }
}

