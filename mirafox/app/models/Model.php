<?php
namespace app\models;
use app\Application;

/**
 * @author oegir
 * Implements data access methods
 */
abstract class Model
{
    /**
     * @var array storage of data providers
     */
    private $_providers = array();

    /**
     * @var array Stores messages to the user
     */
    protected $messages = array();

    /**
     * @var array Parameters for model adjustment
     */
    protected $params;

    /**
     * Loads a specific data provider, if necessary
     *
     * @param string|array $which Data about provider
     * @return app\dataProviders\IDataProvider
     */
    private function _getDataProvider($which)
    {
        if (isset($this->_providers[$which])) {
            $provider = $this->_providers[$which];
        } else {
            $provider = Application::loadObjectWithParams($which, 'data_providers');
            $this->_providers[$which] = $provider;
        }
        return $provider;
    }

    /**
     * Gets data from a storage
     *
     * @param string $which Data area
     * @return array
     */
    protected function getData($which, $params)
    {
        $data_provider = $this->_getDataProvider($which);
        return $data_provider->getData($params);
    }

    /**
     * @param string $which Data area
     * @param array $params
     * @return boolean
     */
    protected function storeData($which, $params)
    {
        $data_provider = $this->_getDataProvider($which);
        return $data_provider->storeData($params);
    }

    public function __construct($params = [])
    {
        $this->params = $params;
    }

    /**
     * Stores user message
     * @param string $message
     */
    public function addMessage($message)
    {
        $this->messages[] = $message;
    }

    /**
     * List of user messages
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }
}
