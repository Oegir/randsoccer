<?php
namespace app\controllers;

use Exception;
use ReflectionClass;
use app\Application;

/**
 *
 * @author oegir
 *         Base controller class
 */
abstract class Controller
{
    /**
     * Current model object
     * @var app\models\Model
     */
    protected $model;

    public abstract function indexAction();

    public function __construct($params = [])
    {
        // model loading
        if (isset($params['model'])) {
            $this->model = Application::loadObjectWithParams($params['model'], 'models');
        }
    }

    /**
     * Executin controller action
     *
     * @param string $action Action name
     */
    public function execute($action = 'index')
    {
        // Add possible message to model
        $message = Application::getGetValue('m', false);
        if ($message) {
            $this->model->addMessage($message);
        }
        // Recognise action
        $action_to_do = Application::getGetValue('a', 'index') . 'Action';
        $reflection = new ReflectionClass($this);

        try {
            $ref_method = $reflection->getMethod($action_to_do);

            if ($ref_method->isPublic()) {
                ob_start();
                $ref_method->invoke($this);
                ob_end_flush();
            } else {
                new Exception('Bad request');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Redirects the user's browser
     * @param string $task
     * @param string $action
     * @param string $message
     */
    public function redirect($controller = '', $action = '', $message = '')
    {
        $url = Application::getUrl($controller, $action);

        if (!empty($message)) {
            $url .= (strripos($url, '?') === false ? '?' : '&') . 'm=' . urlencode($message);
        }
        header('Location: ' . $url);
        die();
    }
}

