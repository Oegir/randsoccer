<?php
namespace app\controllers;

use app\Application;
use Exception;

/**
 *
 * @author oegir
 *
 */
class DefaultController extends Controller
{
    /**
     * Action for initial data loading
     */
    public function indexAction()
    {
        $files = Application::getFiles();

        if (Application::getPostValue('game-play', false)) {
            $this->model->clearHistorical();
        }
        if (!is_null($files)) {
            $this->model->loadSourceData([
                'file_name' => array_values($files)[0],
                'encoding' => Application::getPostValue('encoding', 0),
            ]);
        }
        // Displaying data
        $view = Application::loadObjectWithParams('loadata', 'views', $this->model);
        $view->display();
    }

    /**
     * Action for the draw of teams
     */
    public function drawAction()
    {
        if (Application::getPostValue('game-play', false) && !$this->model->drawCommands()) {
            $this->redirect('', '', 'Не удалось провести жеребьевку');
        }
        $view = Application::loadObjectWithParams('draw', 'views', $this->model);
        $view->display();
    }

    public function roundAction()
    {
        if (Application::getPostValue('game-play', false) && !$this->model->playRounds()) {
            throw new Exception('Ошибка при расчете групповых матчей');
        }
        $view = Application::loadObjectWithParams('rounds', 'views', $this->model);
        $view->display();
    }

    public function finalAction()
    {
        if (Application::getPostValue('restart-final', false)) {
            $this->model->restartFinal();
        }
        if (Application::getPostValue('game-play', false) && !$this->model->playFinal()) {
            throw new Exception('Ошибка при расчете play-off');
        }
        $view = Application::loadObjectWithParams('final', 'views', $this->model);
        $view->display();
    }
}

