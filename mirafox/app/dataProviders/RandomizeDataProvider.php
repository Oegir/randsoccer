<?php
namespace app\dataProviders;

use app\dataProviders\IDataProvider;
use Exception;

/**
 * @author oegir
 *
 */
class RandomizeDataProvider implements IDataProvider
{

    /**
     * @var array Player data storage
     */
    private $_store = array();

    /**
     * @var array
     */
    private $_params;

    /**
     * @var boolean A sign that the game has already been calculated
     */
    private $_played;

    private function _calculateFinal()
    {
        $score = [0, 0];

        while ($score[0] == $score[1]) {
            $score = $this->_calculateGame();
        }
        return $score;
    }

    /**
     * Calculates game parameters
     * @return array Result score of the game
     */
    private function _calculateGame()
    {
        // TODO: Сделать изменение объектов игроков в вызывающем методе на основе полученного счета игры
        if (count($this->_store) != 2) {
            throw new Exception('Unexpected number of players');
        }
        $max_luck = $this->_params['max_luck_factor'];
        $min_luck = $this->_params['min_luck_factor'];
        $range = $this->_params['max_luck_factor'] - $this->_params['min_luck_factor'];
        $strn0 = (int) $this->_store[0]->strength;
        $strn1 = (int) $this->_store[1]->strength;

        $threshold = $max_luck * $this->_params['goal_min_percent'] / 100;

        $luck0 = (int) ($range * $strn0 / 100 + $min_luck);
        $luck1 = (int) ($range * $strn1 / 100 + $min_luck);

        $goals_0 = 0;
        $goals_1 = 0;
        // Drawing of the game score
        for ($i = 0; $i < $this->_params['max_goals']; $i++) {
            $attack0 = mt_rand(0, $luck0);
            $guard1 = mt_rand(0, $luck1);

            if ($attack0 > $threshold && $attack0 > $guard1) {
                $goals_0++;
            }
            $attack1 = mt_rand(0, $luck1);
            $guard0 = mt_rand(0, $luck0);

            if ($attack1 > $threshold && $attack1 > $guard0) {
                $goals_1++;
            }
            $luck0 = (int) ($luck0 / $this->_params['reduce_factor']);
            $luck1 = (int) ($luck1 / $this->_params['reduce_factor']);

            if ($luck0 < $threshold && $luck1 < $threshold) {
                break;
            }
        }
        $this->_played = true;

        return [$goals_0, $goals_1];
    }

    private function _calculateRound()
    {
        $score = $this->_calculateGame();

        $this->_store[0]->scored += $score[0];
        $this->_store[0]->passed += $score[1];

        $this->_store[1]->scored += $score[1];
        $this->_store[1]->passed += $score[0];

        if ($score[0] > $score[1]) {
            $this->_store[0]->wins++;
            $this->_store[0]->score += 3;

            $this->_store[1]->fails++;
        } elseif ($score[1] > $score[0]) {
            $this->_store[1]->wins++;
            $this->_store[1]->score += 3;

            $this->_store[0]->fails++;
        } else {
            $this->_store[0]->draws++;
            $this->_store[0]->score += 1;
            $this->_store[1]->draws++;
            $this->_store[1]->score += 1;
        }

        $this->_store[0]->games++;
        $this->_store[1]->games++;

        return $this->_store;
    }

    public function __construct($params)
    {
        $this->_params = $params;
        $this->_played = false;
    }

    /**
     * {@inheritDoc}
     * @see \app\dataProviders\IDataProvider::storeData()
     */
    public function getData($params)
    {
        $phase = $params;

        if (!$this->_played) {

            switch ($phase) {
                case 'round':
                    return $this->_calculateRound();
                break;

                case 'final':
                    return $this->_calculateFinal();
                break;

                default:
                    throw new Exception('Incorrect game type');
                break;
            }
        }
    }

    /**
     * Play some part of the finale
     */
    public function playFinal()
    {

    }

    /**
     * {@inheritDoc}
     * @see \app\dataProviders\IDataProvider::storeData()
     */
    public function storeData($data)
    {
        if (count($data) == 2) {
            $this->_played = false;
            $this->_store = $data;
            return true;
        } else {
            return false;
        }
    }
}

