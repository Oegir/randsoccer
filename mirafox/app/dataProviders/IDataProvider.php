<?php
namespace app\dataProviders;

/**
 *
 * @author oegir
 *
 * Fixes data access methods
 *
 */
interface IDataProvider
{
    /**
     * Returns data read from storage in an associative array
     * @param array $params Params for adjustment
     * @return array
     */
    public function getData($params);

    /**
     * @param array $params Stores the data in storage
     * @return bool
     */
    public function storeData($params);
}

