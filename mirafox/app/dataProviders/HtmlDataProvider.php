<?php
namespace app\dataProviders;

use DOMDocument;
use Exception;

/**
 *
 * @author oegir
 *
 */
class HtmlDataProvider implements IDataProvider
{
    /**
     * Returns text from element
     * @param \DOMElement $element
     * @return string
     */
    private function getWholeText($element)
    {
        $text = null;

        for ($i = 0; $i < 10; $i++) {
            $element = $element->firstChild;
            $text = $element->wholeText;

            if ($text != null) {
                break;
            }
        }
        return !is_null($text) ? $text : '';
    }

    /**
     * {@inheritdoc}
     */
    public function getData($params)
    {
        // To obtain data, we need additional parameters
        if (count($params) == 0) {
            return [];
        }
        // Correct document encoding
        $text = file_get_contents($params['file_name']);

        if (mb_strpos($text, '<html') === false) {
            $text = '<!DOCTYPE html><html><head><meta charset="'.$params['encoding'].'"></head><body> ' . $text . ' </body></html>';
        }
        // Read document as html
        $doc = new DOMDocument();
        $doc->loadHTML($text, LIBXML_NOWARNING | LIBXML_NOERROR);
        $rows = $doc->getElementsByTagName('tr');
        $result = [];
        // Convert HTML to Array
        foreach ($rows as $row) {
            $cells = $row->getElementsByTagName('td');
            // Find the desired row by the coincidence of the length
            if ($cells->length == $params['row_lenth']) {
                $reading_row = [];
                // Assign cells
                foreach ($cells as $cell) {
                    $reading_row[] = preg_replace("/(^\s+)|(\s+$)/us", "", $this->getWholeText($cell));
                }
                $result[] = $reading_row;
                $counter++;
                // We need only part of the lines
                if(isset($params['max_rows']) && count($result) == $params['max_rows']) {
                    break;
                }
            }
        }
        return $result;
    }

    /**
     * Data storage not available
     */
    public function storeData($params)
    {
        return false;
    }

}

