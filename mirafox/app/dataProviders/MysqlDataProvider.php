<?php
namespace app\dataProviders;

use PDO;
use PDOException;
use ReflectionClass;
use Exception;

/**
 *
 * @author oegir
 *
 */
class MysqlDataProvider implements IDataProvider
{
    /**
     * @var PDO
     */
    private $_connection;

    private function _clearHistorical()
    {
        try {
            $this->_connection->exec('TRUNCATE `historical`;');
        } catch (PDOException $e) {
            return false;
        }
        return true;
    }

    /**
     * Creates table in database
     */
    private function _createTableHistorical()
    {
        $this->_connection->exec(
            <<<MYSQL
CREATE TABLE `historical` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `command` varchar(100) NOT NULL,
  `games` int(11) DEFAULT '0',
  `scored` int(11) DEFAULT '0',
  `passed` int(11) DEFAULT '0',
  `wins` int(11) DEFAULT '0',
  `draws` int(11) DEFAULT '0',
  `fails` int(11) DEFAULT '0',
  `score` int(11) DEFAULT '0',
  `strength` float DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `command_UNIQUE` (`command`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
MYSQL
            );
    }

    /**
     * Creates table in database
     */
    private function _createTableChampionship()
    {
        $this->_connection->exec(
<<<MYSQL
CREATE TABLE `championship` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `command` varchar(100) NOT NULL,
  `games` int(11) DEFAULT '0',
  `scored` int(11) DEFAULT '0',
  `passed` int(11) DEFAULT '0',
  `wins` int(11) DEFAULT '0',
  `draws` int(11) DEFAULT '0',
  `fails` int(11) DEFAULT '0',
  `score` int(11) DEFAULT '0',
  `strength` float DEFAULT '0',
  `group` varchar(5) DEFAULT '0',
  `phase` float DEFAULT NULL,
  `goals` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `command_UNIQUE` (`command`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
MYSQL
        );
    }

    private function _getFinalists()
    {
        $stmnt = $this->_connection->query(
            <<<MySQL
SELECT
*
FROM
`championship`
WHERE
`phase` = (SELECT
    MIN(`phase`)
    FROM
    `championship`);
MySQL
        );
        $result = $stmnt->fetchAll(PDO::FETCH_OBJ);
        return $result;
    }

    /**
     * Returns array of strings with parts of INSERT SQL-query
     * @var array $data
     * @return array
     */
    private function _getInsertParts($data)
    {
        $insert_values = array();
        $query_marks = array();
        // Prepare bulk insert query values
        foreach ($data as $row) {
            $marks = array();

            foreach ($row as $col_name => $cell) {
                $marks[] = '?';
                $insert_values[] = $cell;
            }
            $columns_quoted = true;
            $query_marks[] = '(' . implode(',', $marks) . ')';
        }
        return [
            'columns' => $this->_quoteColumns(array_keys($data[0])),
            'marks' => implode(',', $query_marks),
            'values' => $insert_values
        ];
    }

    /**
     * Returns source data
     * @throws Exception
     * @return array
     */
    private function _getSQLData($table)
    {
        $this->_prepareTable($table);
        $result = $this->_connection->query('SELECT * FROM `' . $table . '`');
        if (!$result) {
            throw new Exception('Can\'t retrieve previously downloaded data');
        }
        return $result->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Clears the Championship Statistics
     */
    private function _nullifyStatistic()
    {
        $nullify_sql = <<<MySQL
UPDATE `championship`
SET
    `games` = '0',
    `scored` = '0',
    `passed` = '0',
    `wins` = '0',
    `draws` = '0',
    `fails` = '0',
    `score` = '0',
    `phase` = NULL
WHERE
    `id` != '-1'
MySQL;
        $this->_connection->exec($nullify_sql);
    }

    /**
     * Checks the existence of a table in the database and creates if it does not exist
     * @param string $table_name
     */
    private function _prepareTable($table_name)
    {
        $stmnt = $this->_connection->prepare('SHOW TABLES LIKE :table_name');
        $stmnt->execute(['table_name' => $table_name]);
        $result = $stmnt->fetch();

        if (!$result) {
            $reflection = new ReflectionClass($this);
            $ref_method = $reflection->getMethod('_createTable' . ucfirst($table_name));
            $ref_method->setAccessible(true);
            $ref_method->invoke($this);
        }
    }

    /**
     * Qouting columns for inset it to SQL-query
     * @param array $columns
     * @return array
     */
    private function _quoteColumns($columns)
    {
        $quoted_columns = array();

        foreach ($columns as $column) {
            $quoted_columns[] = '`' . $column . '`';
        }
        return implode(',', $quoted_columns);
    }

    /**
     * Clears results of last final part
     */
    private function _restartFinal()
    {
        $min_phase = $this->_connection->query('SELECT MIN(`phase`) FROM `championship`')->fetch(PDO::FETCH_NUM);
        $stmnt = $this->_connection->prepare('SELECT `id` FROM `championship` WHERE `phase` = ?');
        $ids = $stmnt->execute($min_phase);
        $ids = $stmnt->fetchAll(PDO::FETCH_ASSOC);
        $place_holders = '';

        for ($i = 0; $i < count($ids); $i++) {
            $place_holders .= '?,';
            $ids[$i] = $ids[$i]['id'];
        }
        array_unshift($ids, $min_phase[0] * 2);
        $place_holders = '(' . rtrim($place_holders, ',') . ')';
        $stmnt = $this->_connection->prepare('UPDATE `championship` SET `phase` = ?, `goals` = 0 WHERE `id` IN ' . $place_holders);
        $stmnt->execute($ids);
    }

    /**
     * Inserts data to MySQL databse
     * @param string $table
     * @param array $data
     * @return boolean
     */
    private function _saveData($table, $data)
    {
        $result = true;
        $parts = $this->_getInsertParts($data);
        $sql = 'INSERT INTO `' . $table . '` (' . $parts['columns'] . ') VALUES ' . $parts['marks'];
        $stmt = $this->_connection->prepare ($sql);

        try {
            $this->_connection->beginTransaction();
            $this->_connection->exec('TRUNCATE `' . $table . '`;');
            $stmt->execute($parts['values']);
            $this->_connection->commit();
        } catch (PDOException $e) {
            $result = false;
            $this->_connection->rollBack();
        }
        return $result;
    }

    /**
     * Save data about draw result
     * @param array $data
     * @return boolean
     */
    private function _saveDraw($data)
    {
        $this->_prepareTable('championship');

        if (count($data) == 0) {
            return false;
        }
        return $this->_saveData('championship', $data);
    }

    /**
     * Seve source data
     * @param array $data
     * @return boolean
     */
    private function _saveHistorical($data) {
        $this->_prepareTable('historical');

        if (count($data) == 0) {
            return true;
        }
        return $this->_saveData('historical', $data);
    }

    /**
     * Save data about games results
     * @param array $data
     * @return boolean
     */
    private function _saveWinners($data)
    {
        $result = true;

        $columns = array_keys((array) $data[0]);
        $sql = 'UPDATE `championship` SET ';

        foreach ($columns as $column) {
            $sql .= '`' . $column . '` = :' . $column . ',';
        }
        $sql = rtrim($sql, ',');
        $sql .= ' WHERE `id` = :id';
        $stmnt = $this->_connection->prepare($sql);

        try {
            $this->_connection->beginTransaction();

            foreach ($data as $row) {
                $to_save = (array) $row;
                $stmnt->execute($to_save);
            }
            $this->_connection->commit();
        } catch (PDOException $e) {
            $result = false;
            $this->_connection->rollBack();
        }
        return $result;
    }

    public function __construct($params)
    {
        $this->_connection = new PDO(
            'mysql:dbname='.$params['database'].';host='.$params['server'].';charset=UTF8', $params['user'], $params['pass']
        );
    }

    /**
     * {@inheritDoc}
     * @see \app\dataProviders\IDataProvider::getData()
     */
    public function getData($params)
    {
        $phase = $params;

        switch ($phase) {
            case 'historical':
                return $this->_getSQLData('historical');
                break;
            case 'rounds':
                $this->_nullifyStatistic();
                // 'break' is omitted intentionally
            case 'championship':
                return $this->_getSQLData('championship');
                break;
            case 'finalists':
                return $this->_getFinalists();
                break;
            default:
                return false;
        }
    }

    /**
     * {@inheritDoc}
     * @see \app\dataProviders\IDataProvider::storeData()
     */
    public function storeData($params)
    {
        $phase = array_keys($params)[0];

        switch ($phase) {
            case 'historical':
                return $this->_saveHistorical($params[$phase]);
            case 'clearHistorical':
                return $this->_clearHistorical();
            case 'draw':
                return $this->_saveDraw($params[$phase]);
            case 'winners':
                return $this->_saveWinners($params[$phase]);
            case 'testart-final':
                return $this->_restartFinal();
            default:
                return false;
        }
    }
}

