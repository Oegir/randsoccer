<?php
namespace app\views;

/**
 * @author oegir
 */
class LoadataView extends View
{
    public function display()
    {
        // Display previous data
        $parms['loaded'] = $this->model->getHistorical();
        $parms['encodings'] = $this->model->getEncodings();
        $parms['messages'] = $this->model->getMessages();
        $this->loadTemplate($parms);
    }

}
