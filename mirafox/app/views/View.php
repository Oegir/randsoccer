<?php
namespace app\views;

use app\Application;
use app\models\Model;
use ReflectionClass;

/**
 * @author oegir
 * Base class for Viwer implementation
 */
abstract class View
{
    /**
     * @var app\models\Model
     */
    protected $model;

    /**
     * Loads Header, Footer and Template
     * @param string $tpl_name
     * @param array $tpl_params
     */
    protected function loadTemplate($tpl_params = [], $tpl_name = '')
    {
        $reflection = new ReflectionClass($this);

        $file_path = Application::getConfigValue('base_path') . $reflection->getNamespaceName() . '/tpl/';
        $file_path = str_replace('\\', '/', $file_path);

        require_once $file_path . 'header.php';

        if (empty($name)) {
            $file_name = strtolower($reflection->getShortName());
            $file_name = str_replace('view', '', $file_name);
        } else {
            $file_name = $tpl_name;
        }
        $file_name = $file_path . $file_name  . '.php';

        if (file_exists($file_name)) {
            extract($tpl_params, EXTR_PREFIX_SAME, "tpl");
            require_once $file_name;
        }
        require_once $file_path . 'footer.php';
    }

    public function __construct($params)
    {
        foreach ($params as $param) {

            if ($param instanceof Model) {
                $this->model = $param;
            }
        }
    }

    /**
     * Displays data from model
     */
    public abstract function display();
}
