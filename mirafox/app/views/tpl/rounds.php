<?php
use app\Application;
?>
<div class="menu">
    <form id="game_control" method="post" action="">
        <input type="hidden" name="game-play" value="true" />
        <input type="button" value="Переиграть" onclick="doAction('<?= Application::getUrl('default', 'round')?>')" />
        <input type="button" value="Запустить плей-офф" onclick="doAction('<?= Application::getUrl('default', 'final')?>')" />
    </form>
</div>

<div>
<?php if (count($messages > 0)) :?>
    <?php foreach ($messages as $message) : ?>
    <p><?= $message ?></p>
    <?php endforeach; ?>
<?php endif;?>
</div>

<?php foreach ($by_groups as $group => $commands):?>
<div style="float: left;padding: 20px">
    <h3>Группа <?= $group ?></h3>
    <table>
        <tr>
            <th style="min-width: 150px;">Команда</th>
            <th>И</th>
            <th>В</th>
            <th>Н</th>
            <th>П</th>
            <th>Голы</th>
            <th>&#177;</th>
            <th>О</th>
        </tr>
    <?php foreach ($commands as $row):?>
    	<tr>
            <td><?= $row->command ?></td>
            <td><?= $row->games ?></td>
            <td><?= $row->wins ?></td>
            <td><?= $row->draws ?></td>
            <td><?= $row->fails ?></td>
            <td><?= $row->scored ?></td>
            <td><?= $row->scored - $row->passed ?></td>
            <td><?= $row->score ?></td>
        </tr>
    <?php endforeach;?>
    </table>
</div>
<?php endforeach; ?>

