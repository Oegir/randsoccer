<?php
use app\Application;
?>
<div class="menu">
<?php if(count($loaded) > 0) : ?>
<div class="menu">
    <form id="game_control" method="post" action="<?= Application::getUrl('default', 'draw')?>">
        <input type="hidden" name="game-play" value="true" />
        <input type="button" value="Очистить" onclick="doAction('<?= Application::getUrl('', '')?>')" />
        <input type="button" value="Запустить жеребьевку" onclick="doAction('<?= Application::getUrl('default', 'draw')?>')" />
    </form>
</div>
<?php endif; ?>
</div>

<form method="post" enctype="multipart/form-data" action="<?= Application::getUrl() ?>">
    <div>
        <input type="hidden" name="MAX_FILE_SIZE" value="30000" />
        <label>Загрузить результаты: <input name="userfile" type="file" /></label>
        <select name="encoding">
<?php foreach ($encodings as $en_key => $en_value) : ?>
            <option value="<?= $en_key ?>"><?= $en_value ?></option>
<?php endforeach; ?>
        </select>
    </div>
    <div>
        <input type="submit" value="Send File" />
    </div>
</form>

<div>
<?php if (count($messages > 0)) :?>
    <?php foreach ($messages as $message) : ?>
    <p><?= $message ?></p>
    <?php endforeach; ?>
<?php endif;?>
</div>

<h2>Данные выступлений</h2>
<table>
    <tr>
        <th style="min-width: 200px;">Команда</th>
        <th>Игры</th>
        <th>Победы</th>
        <th>Ничьи</th>
        <th>Поражения</th>
        <th>Забито</th>
        <th>Пропущено</th>
        <th>Разность</th>
        <th>Очки</th>
        <th>Сила</th>
    </tr>
<?php foreach ($loaded as $row) : ?>
	<tr>
        <td><?= $row->command ?></td>
        <td><?= $row->games ?></td>
        <td><?= $row->wins ?></td>
        <td><?= $row->draws ?></td>
        <td><?= $row->fails ?></td>
        <td><?= $row->scored ?></td>
        <td><?= $row->passed ?></td>
        <td><?= $row->scored - $row->passed ?></td>
        <td><?= $row->score ?></td>
        <td><?= $row->strength ?></td>
    </tr>
<?php endforeach; ?>
</table>
