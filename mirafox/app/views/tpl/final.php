<?php
use app\Application;
?>
<script type="text/javascript">
function doRestart(url) {
	document.getElementById('restart').value = true;
	document.forms.game_control.action = url;
	doAction(url);
}
</script>
<div class="menu">
    <form id="game_control" method="post" action="">
        <input type="hidden" name="game-play" value="true" />
        <input type="hidden" id="restart" name="restart-final" value="false" />
        <input type="button" value="Переиграть" onclick="doRestart('<?= Application::getUrl('default', 'final')?>')" />
<?php if ($final / 2 > 1) :?>
        <input type="button" value="Запустить 1/<?= $final / 2 ?>" onclick="doAction('<?= Application::getUrl('default', 'final')?>')" />
<?php elseif ($final / 2 == 1) :?>
        <input type="button" value="Запустить финал" onclick="doAction('<?= Application::getUrl('default', 'final')?>')" />
<?php endif;?>
    </form>
</div>

<div>
<?php if (count($messages > 0)) :?>
    <?php foreach ($messages as $message) : ?>
    <p><?= $message ?></p>
    <?php endforeach; ?>
<?php endif;?>
</div>

<div style="float: left;padding: 20px">
    <table>
    <?php for ($i = 0; $i < count($commands); $i += 2):?>
    	<tr>
            <td><?= $commands[$i]->command ?></td>
            <td> - </td>
            <td><?= $commands[$i + 1]->command ?></td>
            <td><?= $commands[$i]->goals ?> - <?= $commands[$i + 1]->goals ?></td>
        </tr>
    <?php endfor;?>
    </table>
</div>

