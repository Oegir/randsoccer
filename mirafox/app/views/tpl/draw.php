<?php
use app\Application;
?>
<div class="menu">
    <form id="game_control" method="post" action="<?= Application::getUrl('default', 'round')?>">
        <input type="hidden" name="game-play" value="true" />
        <input type="button" value="Переиграть" onclick="doAction('<?= Application::getUrl('default', 'draw')?>')" />
        <input type="button" value="Запустить групповые матчи" onclick="doAction('<?= Application::getUrl('default', 'round')?>')" />
    </form>
</div>

<div>
<?php if (count($messages > 0)) :?>
    <?php foreach ($messages as $message) : ?>
    <p><?= $message ?></p>
    <?php endforeach; ?>
<?php endif;?>
</div>

<?php foreach ($by_groups as $group => $commands):?>
<div style="float: left;padding: 20px">
    <h3>Группа <?= $group ?></h3>
    <table>
        <tr>
            <th style="min-width: 150px;">Команда</th>
            <th>Группа</th>
        </tr>
    <?php foreach ($commands as $row):?>
    	<tr>
            <td><?= $row->command ?></td>
            <td><?= $group ?></td>
        </tr>
    <?php endforeach;?>
    </table>
</div>
<?php endforeach; ?>

