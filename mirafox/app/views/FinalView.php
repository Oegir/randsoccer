<?php
namespace app\views;

/**
 * @author oegir
 */
class FinalView extends View
{
    public function display()
    {
        $params['final'] = $this->model->getFinal();
        $params['commands'] = $this->model->getList();
        $params['messages'] = $this->model->getMessages();
        $this->loadTemplate($params);
    }

}
