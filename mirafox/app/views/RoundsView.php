<?php
namespace app\views;

/**
 * @author oegir
 */
class RoundsView extends View
{
    public function display()
    {
        $parms['messages'] = $this->model->getMessages();
        $parms['by_groups'] = $this->model->getByGroups();
        $this->loadTemplate($parms);
    }

}
